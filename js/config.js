/**
 * That is the file that will contain the different variables
 * used for the dynamic part of our project
 */

let scroll = new SmoothScroll('a[href*="#"]');
let siteTitle = document.querySelectorAll('.site__name');

let brandLogo = document.querySelector(".logo");
let hamburgerMenu = document.querySelector(".header-burger-menu");
let headerNavLinks = document.querySelector(".header-navlinks");

console.log(hamburgerMenu);
console.log(headerNavLinks);
console.log(brandLogo);

hamburgerMenu.addEventListener("click", (e)=>
{
    console.log("Okay");
    brandLogo.classList.toggle("none");
    headerNavLinks.classList.toggle("appear");
})