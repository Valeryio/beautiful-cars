# Beautiful-cars : 🚗

![Design preview for the challenge](/ressources/Design%20WEB/cover.png)

## Integrate with your tools
No need of integration for now!


## Test and Deploy
No need of test of deployement for now!

## Description
This project is a frontend application about of commercial point of sales of a company that sells car.
Owned by M Robert, it's a showcase website!


## Visuals
No visuals for the moment!


## Authors and acknowledgment
- [@Valeryio](https://gitlab.com/Valeryio)



## Project status
In progress...
